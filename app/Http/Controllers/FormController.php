<?php

namespace App\Http\Controllers;

use App\Models\UserForm;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class FormController extends Controller
{
    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function submit(Request $request): RedirectResponse
    {
        $requestData = $request->validate([
            'name' => 'required|min:2|max:30',
            'email' => 'required|email',
            'message' => 'required|min:10',
        ]);

        try {
            UserForm::create($requestData);
        } catch (Exception $exception) {
            Log::error(
                'Create user form error',
                [
                    'message' => $exception->getMessage()
                ]
            );
        }

        return redirect()->route('home');
    }
}


