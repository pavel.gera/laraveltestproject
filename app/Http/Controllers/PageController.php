<?php

namespace App\Http\Controllers;

use App\Models\UserForm;


class PageController extends Controller
{
    public function home()
    {
        return view(
            'index',
            [
                'data' => UserForm::all()
            ]
        );
    }
}
