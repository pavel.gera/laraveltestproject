@extends('layouts.template')

@section('title')
    Test task
@endsection

@section('content')
<h1>Test task</h1>

<form action={{ route('form-submit') }} method="post" class="form">
    @csrf

    @include('layouts/errors')
    <div class="form-group">
        <label for="name">Name</label>
    <input type="text" name="name" placeholder="Input your name" id="name">
    </div>

    <div class="form-group">
        <label for="email">Email</label>
    <input type="text" name="email" placeholder="Input your email" id="email">
    </div>

    <div class="form-group">
        <label for="message">Message</label>
    <input type="text" name="message" placeholder="Input your message" id="message">
    </div>
    <button class="w-40 btn btn-primary btn-lg" type="submit" name="button">Submit</button>
</form>

@endsection

@section('footer')
    @include('partials.userForms', ['data' => $data])
@endsection
