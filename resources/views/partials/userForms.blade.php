@foreach($data as $elem)
    <div class="list">
        <h4> {{ $elem->name }} </h4>
        <li> {{ $elem->email }} </li>
        <li> {{ $elem->message }} </li>
        <p><small>{{ $elem->created_at }}</small></p>
    </div>
@endforeach
